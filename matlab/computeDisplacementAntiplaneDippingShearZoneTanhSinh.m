function u1=computeDisplacementAntiplaneDippingShearZoneTanhSinh( ...
    x2,x3,q2,q3,T,W,phi,epsv12p,epsv13p)
% function COMPUTEDISPLACEMENTANTIPLANEDIPPINGSHEARZONETANHSINH computes
% the displacement field associated with deforming shear zones
% considering the following geometry using the double-exponential quadrature.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   | dip /
%                   |----/  . w
%                   |   /     . i 
%                   |  /        . d           
%                   | /           . t     
%                   |/              . h   
%           q2,q3 ->@                 .
%                  /|                   . 
%                 / :                  /  
%                /  |                 /  s
%               /   :                /  s
%              /    |               /  e
%             /     :              /  n
%               .   |             /  k
%                 . :            /  c
%                   .           /  i
%                   : .        /  h
%                   |   .     /  t
%                   :     .  /  
%                   |       .    
%                   D (x3)
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% q2, q3             east and depth coordinates of the shear zone,
% T, W               thickness and width of the shear zone,
% epsv12, epsv13     source strain component 22, 23 and 33 in the shear zone
%                    in the system of reference tied to the shear zone
%
% Output:
% u1                 displacement component in the north (along-strike) direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) 

% rotate the eigenstrain from the shear-zone centric to the reference
% coordinate system
epsv12= sind(phi)*epsv12p+cosd(phi)*epsv13p;
epsv13=-cosd(phi)*epsv12p+sind(phi)*epsv13p;

y2=@(y2p,y3p) +y2p*sind(phi)+y3p*cosd(phi)+q2;
y3=@(y2p,y3p) -y2p*cosd(phi)+y3p*sind(phi)+q3;

% Green's functions
G11=@(y2,y3) log((x2-y2).^2+(x3-y3).^2)+log((x2-y2).^2+(x3+y3).^2);

% function IU1 is the integrand for displacement component u1
IU1=@(x) -1/2/pi*( ...
    sind(phi)*( epsv12*W/2*(G11(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G11(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
               +epsv13*T/2*(G11(y2(x*T/2,W),      y3(x*T/2,W))      -G11(y2(x*T/2,0),       y3(x*T/2,0)))) ...
   +cosd(phi)*( epsv12*T/2*(G11(y2(x*T/2,W),      y3(x*T/2,W))      -G11(y2(x*T/2,0),       y3(x*T/2,0))) ...
               -epsv13*W/2*(G11(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G11(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2)))) ...
               );

% numerical solution
h=0.01;
n=fix(1/h*3);
u1=zeros(size(x2));

for k=-n:n
    wk=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
    xk=tanh(0.5*pi*sinh(k*h));
    u1=u1+wk*IU1(xk);
end

end


