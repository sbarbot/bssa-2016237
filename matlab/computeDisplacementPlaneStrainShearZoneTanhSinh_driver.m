
% prescribe strain in the shear-zone-centric coordinate system
epsv22p=0e-6;
epsv23p=1e-6;
epsv33p=0e-6;

% dimension
W=20e3; % width down dip
T=10e3; % thickness

% position
q2=0;
q3=20e3; % depth

% orientation
phi=90;  % dip angle

% elastic structure
G=1;     % rigidity
nu=0.25; % Poisson's ratio

% observation points
N=128;
dx2=1e3;
dx3=1e3;
x2=repmat((-N/2:(N/2)-1)'*dx2,1,N);
x3=repmat((0:N-1)*dx2,N,1);

tic
[u2n,u3n]=computeDisplacementPlaneStrainShearZoneTanhSinh( ...
    x2,x3,q2,q3,T,W,phi,epsv22p,epsv23p,epsv33p,G,nu);
[u2a,u3a]=computeDisplacementPlaneStrainVerticalShearZone( ...
    x2,x3,q2,q3,T,W,epsv22p,epsv23p,epsv33p,G,nu);
toc

%% plot displacements
figure(1);clf;
subplot(2,2,1);cla;hold on;
pcolor(x2/1e3,x3/1e3,u2n-u2a),shading flat;
h=colorbar();
ylabel(h,'u2');
set(gca,'YDir','reverse');
axis equal tight
box on, grid on
xlabel('x_2 (km)');
ylabel('x_3 (km)');

subplot(2,2,3);cla;hold on;
pcolor(x2/1e3,x3/1e3,u3n-u3a),shading flat;
h=colorbar();
ylabel(h,'u_3');
set(gca,'YDir','reverse');
axis equal tight
box on, grid on
xlabel('x_2 (km)');
ylabel('x_3 (km)');


subplot(2,2,2);cla;hold on;
pcolor(x2/1e3,x3/1e3,u2n),shading flat;
h=colorbar();
ylabel(h,'u2');
set(gca,'YDir','reverse');
axis equal tight
box on, grid on
xlabel('x_2 (km)');
ylabel('x_3 (km)');

subplot(2,2,4);cla;hold on;
pcolor(x2/1e3,x3/1e3,u3n),shading flat;
h=colorbar();
ylabel(h,'u_3');
set(gca,'YDir','reverse');
axis equal tight
box on, grid on
xlabel('x_2 (km)');
ylabel('x_3 (km)');

figure(62);clf;
cla;hold on;
axis equal tight
down=2;
set(gca,'Clipping','on');
pcolor(x2/1e3,x3/1e3,sqrt(u2n.^2+u3n.^2)),shading flat;
quiver(x2(1:down:end,1:down:end)/1e3,x3(1:down:end,1:down:end)/1e3, ...
    u2(1:down:end,1:down:end),u3(1:down:end,1:down:end));
h=colorbar();
ylabel(h,'Displacement amplitude');
set(gca,'YDir','reverse');
%set(gca,'xlim',[-1 1]*60,'ylim',[0 1]*120);
box on, grid on
xlabel('x_2 (km)');
ylabel('x_3 (km)');
