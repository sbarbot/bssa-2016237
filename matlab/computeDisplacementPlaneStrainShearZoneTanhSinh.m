function [u2,u3]=computeDisplacementPlaneStrainShearZoneTanhSinh( ...
    x2,x3,q2,q3,T,W,phi,epsv22p,epsv23p,epsv33p,G,nu)
% function COMPUTEDISPLACEMENTPLANESTRAINSHEARZONETANHSINH computes the
% displacement field associated with deforming vertical shear zones
% considering the following geometry using the tanh-sinh numerical
% quadrature.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   | dip /
%                   |----/  . w
%                   |   /     . i 
%                   |  /        . d           
%                   | /           . t     
%                   |/              . h   
%           q2,q3 ->@                 .
%                  /|                   . 
%                 / :                  /  
%                /  |                 /  s
%               /   :                /  s
%              /    |               /  e
%             /     :              /  n
%               .   |             /  k
%                 . :            /  c
%                   .           /  i
%                   : .        /  h
%                   |   .     /  t
%                   :     .  /  
%                   |       .    
%                   D (x3)
%
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% q2, q3             east and depth coordinates of the shear zone,
% T, W               thickness and width of the shear zone,
% phi (degree)       dip angle from horizontal of the shear zone,
% epsvij             source strain component 22, 23 and 33 in the shear zone
%                    in the system of reference tied to the shear zone,
% G, nu              shear modulus and Poisson's ratio in the half space.
%
% Output:
% u2                 displacement component in the east direction,
% u3                 displacement component in the down direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Feb 22, 2016

% Lame parameter
lambda=G*2*nu/(1-2*nu);

% array size
s=size(x2);

% convert strain to reference coordinate system R=[[sind(phi), cosd(phi)];[-cosd(phi), sind(phi)]];
epsv22=+sind(phi)*(+epsv22p*sind(phi)+epsv23p*cosd(phi))+cosd(phi)*(+epsv23p*sind(phi)+epsv33p*cosd(phi));
epsv23=+sind(phi)*(-epsv22p*cosd(phi)+epsv23p*sind(phi))+cosd(phi)*(-epsv23p*cosd(phi)+epsv33p*sind(phi));
epsv33=-cosd(phi)*(-epsv22p*cosd(phi)+epsv23p*sind(phi))+sind(phi)*(-epsv23p*cosd(phi)+epsv33p*sind(phi));

% isotropic strain
epsvkk=epsv22+epsv33;

% Green's functions
r1=@(y2,y3) sqrt((x2-y2).^2+(x3-y3).^2);
r2=@(y2,y3) sqrt((x2-y2).^2+(x3+y3).^2);

y2=@(y2p,y3p) +y2p*sind(phi)+y3p*cosd(phi)+q2;
y3=@(y2p,y3p) -y2p*cosd(phi)+y3p*sind(phi)+q3;

G22=@(y2,y3) -1/(2*pi*G*(1-nu))*( ...
    (3-4*nu)/4*log(r1(y2,y3))+(8*nu^2-12*nu+5)/4*log(r2(y2,y3))+(x3-y3).^2./r1(y2,y3).^2/4 ...
    +((3-4*nu)*(x3+y3).^2+2*y3.*(x3+y3)-2*y3.^2)./r2(y2,y3).^2/4-(y3.*x3.*(x3+y3).^2)./r2(y2,y3).^4 ...
    );
G23=@(y2,y3) 1/(2*pi*G*(1-nu))*( ...
    (1-2*nu)*(1-nu)*atan((x2-y2)./(x3+y3))+((x3-y3).*(x2-y2))./r1(y2,y3).^2/4 ...
    +(3-4*nu)*((x2-y2).*(x3-y3))./r2(y2,y3).^2/4-(y3.*x3.*(x2-y2).*(x3+y3))./r2(y2,y3).^4 ...
    );
G32=@(y2,y3) 1/(2*pi*G*(1-nu))*( ...
    -(1-2*nu)*(1-nu)*atan((x2-y2)./(x3+y3))+(x3-y3).*(x2-y2)./r1(y2,y3).^2/4 ...
    +(3-4*nu)*(x2-y2).*(x3-y3)./r2(y2,y3).^2/4+y3.*x3.*(x2-y2).*(x3+y3)./r2(y2,y3).^4 ...
    );
G33=@(y2,y3) 1/(2*pi*G*(1-nu))*( ...
    -(3-4*nu)/4*log(r1(y2,y3))-(8*nu^2-12*nu+5)/4*log(r2(y2,y3)) ...
    -(x2-y2).^2./r1(y2,y3).^2/4+(2*y3.*x3-(3-4*nu)*(x2-y2).^2)./r2(y2,y3).^2/4-y3.*x3.*(x2-y2).^2./r2(y2,y3).^4 ...
     );

% function IU2 is the integrand for displacement component u2
IU2=@(x) ...
    sind(phi)*( ...
         (lambda*epsvkk+2*G*epsv22)*W/2*(G22(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G22(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
                        +2*G*epsv23*T/2*(G22(y2(x*T/2,W),y3(x*T/2,W))            -G22(y2(x*T/2,0),y3(x*T/2,0))) ...
        +(lambda*epsvkk+2*G*epsv33)*T/2*(G32(y2(x*T/2,W),y3(x*T/2,W))            -G32(y2(x*T/2,0),y3(x*T/2,0))) ...
                        +2*G*epsv23*W/2*(G32(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G32(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ) ...
   +cosd(phi)*( ...
         (lambda*epsvkk+2*G*epsv22)*T/2*(G22(y2(x*T/2,W),y3(x*T/2,W))            -G22(y2(x*T/2,0),y3(x*T/2,0))) ...
                        -2*G*epsv23*W/2*(G22(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G22(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
        -(lambda*epsvkk+2*G*epsv33)*W/2*(G32(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G32(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
                        +2*G*epsv23*T/2*(G32(y2(x*T/2,W),y3(x*T/2,W))            -G32(y2(x*T/2,0),y3(x*T/2,0))) );

% function IU3 is the integrand for displacement component u3
IU3=@(x) ...
    sind(phi)*( ...
         (lambda*epsvkk+2*G*epsv22)*W/2*(G23(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G23(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
                        +2*G*epsv23*T/2*(G23(y2(x*T/2,W),y3(x*T/2,W))            -G23(y2(x*T/2,0),y3(x*T/2,0))) ...
        +(lambda*epsvkk+2*G*epsv33)*T/2*(G33(y2(x*T/2,W),y3(x*T/2,W))            -G33(y2(x*T/2,0),y3(x*T/2,0))) ...
                        +2*G*epsv23*W/2*(G33(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G33(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ) ...
   +cosd(phi)*( ...
         (lambda*epsvkk+2*G*epsv22)*T/2*(G23(y2(x*T/2,W),y3(x*T/2,W))            -G23(y2(x*T/2,0),y3(x*T/2,0))) ...
                        -2*G*epsv23*W/2*(G23(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G23(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
        -(lambda*epsvkk+2*G*epsv33)*W/2*(G33(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G33(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
                        +2*G*epsv23*T/2*(G33(y2(x*T/2,W),y3(x*T/2,W))            -G33(y2(x*T/2,0),y3(x*T/2,0))) );

% numerical solution
h=0.01;
n=fix(1/h*3);
u2=zeros(s);
u3=zeros(s);
for k=-n:n
    wk=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
    xk=tanh(0.5*pi*sinh(k*h));
    u2=u2+wk*IU2(xk);
    u3=u3+wk*IU3(xk);
end

end
